package sad;




import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.net.estimate.SimpleEstimator;
import weka.classifiers.bayes.net.search.local.TAN;
import weka.classifiers.meta.FilteredClassifier;


	public class Principal {
		
	    public static void main(String[] args) throws Exception {
	    	Lectura lect= new Lectura();	    	
			Results resultados = new Results();
			System.out.println("Empezamos con la manipulacion de los datos");
	    	/////////////Lectura de datos y aplica el filtro RANDOMIZE/////////////
	    	Instances dataTrain;
	    	dataTrain = lect.cargarDatos(args[0]);
	    	
	    	Instances dataTest;
	    	dataTest = lect.cargarDatos(args[1]);
	    	
	    	//creamos el filtro bayesNet con los patrones siguientes para el stimador y el algortimo de busqueda
	    	SimpleEstimator estimadorBayes = new SimpleEstimator();
	    	TAN tan = new TAN();
	    	BayesNet bayesNet = new BayesNet();
	    	bayesNet.setEstimator(estimadorBayes);
	    	bayesNet.setSearchAlgorithm(tan);
	    	
	    	//cramos el multifilter
	    	MultiFilter multiFilter = new MultiFilter();
	    	Filter[] filter= new Filter[2];//le decimos que va a tener una tama�o de dos puesto que seran 2 filtros
	    	
	    	//creamos el StringWordToVector
	    	StringToWordVector stwv = new StringToWordVector(); 
	    	stwv.setIDFTransform(true);
	    	stwv.setTFTransform(true);
	    	stwv.setOutputWordCounts(true);
	    	filter[0]=stwv;
	    	//creamos el attributeselection
	    	AttributeSelection atrSel = new AttributeSelection();
	    	InfoGainAttributeEval inga = new InfoGainAttributeEval();
	    	atrSel.setEvaluator(inga);
	    	Ranker ranker = new Ranker();
	    	ranker.setNumToSelect(261);//segun el atr solo 261atributos tiene algo de peso con la clase
	    	atrSel.setSearch(ranker);
	    	filter[1]=atrSel;		
	    	multiFilter.setFilters(filter);
	    	
	    	//creamos el clasificador
	    	FilteredClassifier clasificador = new FilteredClassifier();
	    	clasificador.setClassifier(bayesNet);
	    	clasificador.setFilter(multiFilter);
	    	
	    	//preparamos el train vs test
			clasificador.buildClassifier(dataTrain);
			Evaluation evaluator = new Evaluation(dataTrain);
			evaluator.evaluateModel(clasificador, dataTest);
			System.out.println("Creando el archivo de las predicciones,NOTA se creara junto al ejecutable.");
			//imprimimos los resultados
			resultados.imprimirResultados(dataTrain, evaluator);
			System.out.println("Predicciones terminadas, compruebe el archivo.");
	    }
}


