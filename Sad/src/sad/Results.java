package sad;

import java.io.FileWriter;
import java.io.PrintWriter;

import weka.classifiers.Evaluation;
import weka.core.Instances;

public class Results {

	public void imprimirResultados(Instances dataSel, Evaluation evaluator) throws Exception {
		FileWriter fichero = null;
		PrintWriter pw = null;
		String prediccion;
		String[] predict;
		int z = 0;
		try {
			fichero=new FileWriter ("F:/descargas/datos/movies_reviews/ejercicio_5/predicciones_IDF_TFT.txt");
			pw= new PrintWriter(fichero);
			pw.println(
					"N�instancia " + " Clase actual " + " Clase clasificada " + " %clase POS " + " %clase NEG");
			while (z < 400) {
				prediccion = evaluator.predictions().elementAt(z).toString();
				prediccion = prediccion.replace("NOM:", "");
				prediccion = prediccion.replace("NaN", "?");
				predict = prediccion.split(" ");
				if (predict[4].contains("E")) {// significa que el numero es muy
												// peque�o, weka lo redondea a 0
												// y 1
					predict[4] = "  0  ";
					predict[5] = "  1  ";
				} else {
					predict[4] = predict[4].substring(0, 5);
				}
				if (predict[5].contains("E")) {// significa que el numero es muy
												// peque�o, weka lo redondea a 0
												// y 1
					predict[5] = "  0  ";
					predict[4] = "  1  ";
				} else {
					predict[5] = predict[5].substring(0, 5);
				}
				int t = z + 1;
				if (predict[2].equals("0.0")) {
					predict[2] = "Pos";
				} else {
					predict[2] = "Neg";
				}
				pw.println("     " + t + "           " + predict[1] + "              " + predict[2]
						+ "            " + predict[4] + "       " + predict[5]);
				z++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
