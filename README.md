# README #

Parte del trabajo final de SAD(sistemas de apoyo a la decisión). 

### ¿Que hace este repositorio? ###

* Se le pasan dos ficheros .arf uno con películas catalogadas como buenas o malas según una opinión y otro archivo con películas y opiniones pero sin catalogar.
* El programa aplica filtros y algoritmos para aprender un patrón de como catalogar las películas según su opinión.
* El programa aplica ese patrón aprendido a las películas sin catalogar para establecer si son buenas o malas.

### ¿Como funciona este repositorio? ###

* El programa lee los dos ficheros .arff
* Les aplica el filtro bayesiano (bayesnet).
* Se crea el clasificador "multifilter" y se procesan los datos.
* El programa estima si las películas son buenas o malas según su opinión usando trainVstest.
* El programa crea un fichero con las películas ya catalogadas.

### Tecnología ###

* Java
* API de Weka(http://www.cs.waikato.ac.nz/ml/weka/)

### Creador ###

* Jonathan Guijarro Garcia